       IDENTIFICATION DIVISION.
       PROGRAM-ID.    GDSN001A.
       AUTHOR.        AMA.
       DATE-WRITTEN.  JUILLET 2015.
       DATE-COMPILED.

      *************************************************************
      *                                                           *
      *    APPAREILLAGE ENTRE FICHIER REDAC ET FICHIER SUIVI      *
      *                                                           *
      *    + FICHIER EN ENTREE :                                  *
      *    DDE1S  : FICHIER SUIVI (DETAILS DES REJETS RENCONTRES) *
      *    DDE2S  : FICHIER REDAC                                 *
      *                                                           *
      *    + FICHIER EN SORTIE :                                  *
      *    DDS1S  : FICHIER REDAC (SANS LIGNES EN REJET)          *
      *                                                           *
      *************************************************************
      * 20/07/15 *      CREATION DU PROGRAMME     MANTIS 1854     *
      *   AMA    *                                                *
      *************************************************************

       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
      *---------------------*
       OBJECT-COMPUTER. IBM-3090.
       SOURCE-COMPUTER. IBM-3090.
      *SOURCE-COMPUTER. IBM-3090 WITH DEBUGGING MODE.

       INPUT-OUTPUT SECTION.
      *--------------------*
       FILE-CONTROL.
           SELECT  FIC-ENT1 ASSIGN TO  DDE1S.
           SELECT  FIC-ENT2 ASSIGN TO  DDE2S.
           SELECT  FIC-SOR  ASSIGN TO  DDS1S.

       DATA DIVISION.
      *-------------*
       FILE SECTION.

      * FICHIER SUIVI
       FD FIC-ENT1
            RECORDING MODE IS F
            LABEL RECORDS STANDARD
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-ENT1                    PIC X(400).
      * FICHIER REDAC
       FD FIC-ENT2
            RECORDING MODE IS V
            RECORD VARYING 1 TO 2500 DEPENDING WS-LG-ENT
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-ENT2                    PIC X(2500).

      * FICHIER REDAC (LIGNES NON TROUVEES DANS SUIVI)
       FD FIC-SOR
            RECORDING MODE IS V
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-SOR.
           03 ENR-SOR PIC X(1) OCCURS 1 TO 2500 DEPENDING WS-LG-SOR.


       WORKING-STORAGE SECTION.
      *-----------------------*
       01  STAT-PROG.
           05  STAT-PROG-DATE-EXEC.
               10  STAT-PROG-DATE-EXEC-AA      PIC 9(02).
               10  STAT-PROG-DATE-EXEC-MM      PIC 9(02).
               10  STAT-PROG-DATE-EXEC-JJ      PIC 9(02).
           05  STAT-PROG-TIME-EXEC.
               10  STAT-PROG-TIME-EXEC-HH      PIC 9(02).
               10  STAT-PROG-TIME-EXEC-MN      PIC 9(02).
               10  STAT-PROG-TIME-EXEC-SS      PIC 9(02).
               10  STAT-PROG-TIME-EXEC-CN      PIC 9(02).
           05  STAT-PROG-DATE-COMP.
               10  STAT-PROG-DATE-COMP-MM      PIC 9(02).
               10  FILLER                      PIC X(01).
               10  STAT-PROG-DATE-COMP-JJ      PIC 9(02).
               10  FILLER                      PIC X(01).
               10  STAT-PROG-DATE-COMP-AA      PIC 9(02).
               10  STAT-PROG-TIME-COMP-HH      PIC 9(02).
               10  FILLER                      PIC X(01).
               10  STAT-PROG-TIME-COMP-MN      PIC 9(02).
               10  FILLER                      PIC X(01).
               10  STAT-PROG-TIME-COMP-SS      PIC 9(02).

      **ZONES DIVERSES
       01  TIRET-120          PIC X(120) VALUE ALL '-'.

      ** POUR FICHIERS VARIABLES (E/S)
       01 WS-LG-VB.
           03 WS-LG-ENT                 PIC 9(04) COMP.
           03 WS-LG-SOR                 PIC 9(04) COMP.

      ** ZONE COMPTEURS
       01 WS-CPT.
           03 WS-CPT-LIGNE-E1           PIC S9(11) COMP-3.
           03 WS-CPT-LIGNE-E2           PIC S9(11) COMP-3.
           03 WS-CPT-LIGNE-S1           PIC S9(11) COMP-3.

      ** ZONE DE TRAVAIL
       01 WS-KEYS.
           03 WS-KEY-E1.
               05 WS-E1-CONT            PIC X(15) VALUE SPACES.
               05 WS-E1-DATE-DEB        PIC 9(08) VALUE ZEROES.
               05 WS-E1-DATE-FIN        PIC 9(08) VALUE ZEROES.
           03 WS-KEY-E2.
               05 WS-E2-CONT            PIC X(15) VALUE SPACES.
               05 WS-E2-DATE-DEB        PIC 9(08) VALUE ZEROES.
               05 WS-E2-DATE-FIN        PIC 9(08) VALUE ZEROES.

       01 WS-ETAT-FICHIERS.
           03 ETAT-FICHIER-E1           PIC X(01).
               88 WS-E1-EOF                       VALUE 'O'.
               88 WS-E1-OK                        VALUE 'N'.
           03 ETAT-FICHIER-E2           PIC X(01).
               88 WS-E2-EOF                       VALUE 'O'.
               88 WS-E2-OK                        VALUE 'N'.

       COPY GDSNSUIV REPLACING ==(PREF-)== BY ==E1-==.
       COPY GDSN005A REPLACING ==(PREF-)== BY ==E2-==.


       PROCEDURE DIVISION.
      ********************
           DISPLAY '*** DEBUT DU PROGRAMME GDSN001A ***'

           PERFORM INITIALISATION

           PERFORM LECTURE-DDE1S
           PERFORM LECTURE-DDE2S

           PERFORM UNTIL WS-E2-EOF
              EVALUATE TRUE
               WHEN E1-TYPE-ENREG NOT = 02
                   PERFORM LECTURE-DDE1S
               WHEN WS-KEY-E1 = WS-KEY-E2
                   PERFORM LECTURE-DDE2S
               WHEN WS-KEY-E1 < WS-KEY-E2
                   PERFORM LECTURE-DDE1S
               WHEN OTHER
                   PERFORM ECRITURE-DDS1S
                   PERFORM LECTURE-DDE2S
              END-EVALUATE
           END-PERFORM

           CLOSE FIC-ENT1
           CLOSE FIC-ENT2
           CLOSE FIC-SOR
           PERFORM COMPTEURS-PROGRAMME
           DISPLAY '*** FIN   DU PROGRAMME GDSN001A ***'
           GOBACK
           .

       INITIALISATION.
      D    DISPLAY '001A- ** INITIALISATION **'
           PERFORM DISPLAY-PROGRAMME

           OPEN INPUT  FIC-ENT1
           OPEN INPUT  FIC-ENT2
           OPEN OUTPUT FIC-SOR

           SET WS-E1-OK                       TO TRUE
           SET WS-E2-OK                       TO TRUE

           INITIALIZE WS-CPT
           .

       LECTURE-DDE1S.
      D    DISPLAY '001A- ** LECTURE-DDE1S **'

           READ FIC-ENT1                      INTO E1-GDSNSUIV
               AT END
                   SET WS-E1-EOF              TO TRUE
                   MOVE HIGH-VALUE            TO WS-KEY-E1
                   MOVE 2                     TO E1-TYPE-ENREG
               NOT AT END
                   ADD  1                     TO WS-CPT-LIGNE-E1
      *            KEY DDE1S
                   MOVE E1-CP-CONTRAT         TO WS-E1-CONT
                   MOVE E1-CP-DATE-DEB-PER    TO WS-E1-DATE-DEB
                   MOVE E1-CP-DATE-FIN-PER    TO WS-E1-DATE-FIN
           END-READ
           .

       LECTURE-DDE2S.
      D    DISPLAY '001A- ** LECTURE-DDE2S **'

           READ FIC-ENT2                      INTO E2-GDSN005A
               AT END
                   SET WS-E2-EOF              TO TRUE
                   MOVE HIGH-VALUE            TO WS-KEY-E2
               NOT AT END
                   ADD  1                     TO WS-CPT-LIGNE-E2
      *            KEY DDE2S
                   MOVE E2-ECP-NU-CONT        TO WS-E2-CONT
                   MOVE E2-ECP-DATE-DEB-PER   TO WS-E2-DATE-DEB
                   MOVE E2-ECP-DATE-FIN-PER   TO WS-E2-DATE-FIN
           END-READ
           .

       ECRITURE-DDS1S.
      D    DISPLAY '001A- ** ECRITURE-DDS1S **'

           MOVE WS-LG-ENT                     TO WS-LG-SOR
           WRITE F-ENR-SOR                    FROM E2-GDSN005A
           ADD  1                             TO WS-CPT-LIGNE-S1
           .
      *------------------------------------------------------------*
       COMPTEURS-PROGRAMME.
      *------------------------------------------------------------*

           DISPLAY ' '
           DISPLAY '****************************'
           DISPLAY '        COMPTEURS           '
           DISPLAY '****************************'
           DISPLAY ' '
           DISPLAY 'LIGNE E1...................: ' WS-CPT-LIGNE-E1
           DISPLAY 'LIGNE E2...................: ' WS-CPT-LIGNE-E2
           DISPLAY 'LIGNE ECRITS S1............: ' WS-CPT-LIGNE-S1
           DISPLAY ' '
           .

      *------------------------------------------------------------*
       DISPLAY-PROGRAMME.
      *------------------------------------------------------------*
             ACCEPT STAT-PROG-DATE-EXEC FROM DATE.
             ACCEPT STAT-PROG-TIME-EXEC FROM TIME.

             MOVE WHEN-COMPILED         TO STAT-PROG-DATE-COMP

             DISPLAY TIRET-120.
             DISPLAY ' STATISTIQUES DU PROGRAMME GDSN001A'
             DISPLAY TIRET-120.
             DISPLAY 'EXECUTION '
                     ' LE ' STAT-PROG-DATE-EXEC-JJ
                     '/'    STAT-PROG-DATE-EXEC-MM
                     '/'    STAT-PROG-DATE-EXEC-AA
                     ' A '  STAT-PROG-TIME-EXEC-HH
                     ':'    STAT-PROG-TIME-EXEC-MN
                     ':'    STAT-PROG-TIME-EXEC-SS
                     ' , DERNIERE COMPILATION '
                     ' LE ' STAT-PROG-DATE-COMP-JJ
                     '/'    STAT-PROG-DATE-COMP-MM
                     '/'    STAT-PROG-DATE-COMP-AA
                     ' A '  STAT-PROG-TIME-COMP-HH
                     ':'    STAT-PROG-TIME-COMP-MN
                     ':'    STAT-PROG-TIME-COMP-SS
             DISPLAY TIRET-120
             DISPLAY ' '
             .
