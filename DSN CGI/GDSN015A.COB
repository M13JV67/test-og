      *RETRIEVAL
      *DMLIST
      *NO-ACTIVITY-LOG
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    GDSN015A.
       AUTHOR.        AMA.
       DATE-WRITTEN.  JUILLET 2015.
       DATE-COMPILED.

      *************************************************************
      *                                                           *
      *    CONTROLE DES PERIODES SUR UN MEME CONTRAT              *
      *                                                           *
      *    + FICHIER EN ENTREE :                                  *
      *    DDE1S  : FICHIER REDAC                                 *
      *                                                           *
      *************************************************************
      * 29/07/15 *      CREATION DU PROGRAMME     MANTIS 1854     *
      *   AMA    *                                                *
      *************************************************************
      * 15/02/17 *      SUPPRESSION DU NIR POUR LES CODES :       *
      *   EAY    *      G97 - G98 - G99    QDS-671 - Find D00670  *
      *************************************************************

       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
      *---------------------*
       OBJECT-COMPUTER. IBM-3090.
       SOURCE-COMPUTER. IBM-3090.
      *SOURCE-COMPUTER. IBM-3090 WITH DEBUGGING MODE.

       IDMS-CONTROL SECTION.
      *--------------------*
       PROTOCOL.
           MODE IS BATCH-AUTOSTATUS
           IDMS-RECORDS MANUAL.

       INPUT-OUTPUT SECTION.
      *--------------------*
       FILE-CONTROL.
           SELECT  FIC-ENT ASSIGN TO  DDE1S.

       DATA DIVISION.
      *-------------*
       FILE SECTION.

      * FICHIER REDAC
       FD FIC-ENT
            RECORDING MODE IS V
            RECORD VARYING 1 TO 2500 DEPENDING WS-LG-ENT
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-ENT   PIC X(2500).

       SCHEMA SECTION.
      *--------------*
       DB G002S004 WITHIN G002H.


       WORKING-STORAGE SECTION.
      *-----------------------*
      ** POUR FICHIERS VARIABLES (E/S)
       01 WS-LG-VB.
           03 WS-LG-ENT                 PIC 9(04) COMP.

      ** ZONE COMPTEURS
       01 WS-CPT.
           03 WS-CPT-LIGNE              PIC 9(08).

      ** ZONES DATES
       01 WS-DATES.
      *    DATE DU JOUR
           03 WS-DT-TRT.
               05 WS-DT-SA.
                   07  WS-DT-SS         PIC 9(02)  VALUE ZEROES.
                   07  WS-DT-AA         PIC 9(02)  VALUE ZEROES.
               05 WS-DT-SA-9   REDEFINES WS-DT-SA PIC 9(04).
               05 WS-DT-MM              PIC 9(02)  VALUE ZEROES.
               05 WS-DT-JJ              PIC 9(02)  VALUE ZEROES.
           03 WS-DT-TRT-9      REDEFINES WS-DT-TRT PIC 9(08).
      *    DATE D'ECHEANCE
           03 WS-DT-DEB-OK.
               05 WS-DEB-OK-SA.
                   07  WS-DEB-OK-SS     PIC 9(02)  VALUE ZEROES.
                   07  WS-DEB-OK-AA     PIC 9(02)  VALUE ZEROES.
               05 WS-DEB-OK-SA-9   REDEFINES WS-DEB-OK-SA PIC 9(04).
               05 WS-DEB-OK-MM          PIC 9(02)  VALUE ZEROES.
               05 WS-DEB-OK-JJ          PIC 9(02)  VALUE ZEROES.
           03 WS-DT-DEB-OK-9       REDEFINES WS-DT-DEB-OK PIC 9(08).

      ** GESTION ETAT FICHIER ET STATUT IDMS
       01 WS-FLAG.
      **   ETAT CODE REJET
           03 REJECT-STATUS-97          PIC X.
               88 WS-G97-NOT-EXIST                 VALUE 'O'.
               88 WS-G97-EXIST                     VALUE 'N'.
           03 REJECT-STATUS-98          PIC X.
               88 WS-G98-NOT-EXIST                 VALUE 'O'.
               88 WS-G98-EXIST                     VALUE 'N'.
      **   ETAT LECTURE FICHIER
           03 FILE-STATUS               PIC X.
               88 WS-EOF                           VALUE 'O'.
               88 WS-OK                            VALUE 'N'.
      **   IDMS RECORDS STATUS
           03 WS-REC-ECON-STATUS        PIC X.
               88 REC-ECON-OK                      VALUE 'O'.
               88 REC-ECON-KO                      VALUE 'N'.
           03 WS-REC-ESC1-STATUS        PIC X.
               88 REC-ESC1-OK                      VALUE 'O'.
               88 REC-ESC1-KO                      VALUE 'N'.

      ** ZONES VALEURS FIXES
       01 WS-FIXE-VALUE.
      **   APPEL DYNAMIQUE DE PROGRAMMES
           03 APP-DYN.
               05 DATHEUR               PIC X(08) VALUE 'DATHEUR '.
               05 GDSNSUIV              PIC X(08) VALUE 'GDSNSUIV'.
      **   LIBELLE REJET PERIODE
           03 LIB-REJ-PER.
               05 LIB-G97               PIC X(89) VALUE
               'La p{riode @ traiter ne correspond pas @ la p{riode qui
      -        'suit la derni}re p{riode trait{e.'.
               05 LIB-G98               PIC X(83) VALUE
               'Pour un contrat et un fichier, un flux initial et un flu
      -        'x de r{gul ont {t{ trouv{s.'.
               05 LIB-G99               PIC X(66) VALUE
               'Pour un m�me contrat, il existe une intersection entre 2
      -        ' p{riodes.'.
               05 LIB-GPE               PIC X(33) VALUE
               'Rejet suite @ contr�les p{riodes.'.

      ** ZONES TRAVAIL DDS1S
       01 WS-DATA-CSV.
           03 WS-DATA                   PIC X(1000).
           03 WS-LEN                    PIC 9(04) COMP.
       01 WS-DATA-REJET.
           03 WS-COD-REJ                PIC X(05).
           03 WS-LIB-REJ                PIC X(160).

      ** ZONE DATHEUR
       01 WS-DATHEUR.
      **   DAT�ADJM
           03 ZONE-DONNEE-ADJM.
              05 DT-DEPART-ADJM.
                 07 DT-DEP-SS-ADJM      PIC 9(02).
                 07 DT-DEP-AA-ADJM      PIC 9(02).
                 07 DT-DEP-MM-ADJM      PIC 9(02).
                 07 DT-DEP-JJ-ADJM      PIC 9(02).
              05 DT-DEPART-ADJM-9 REDEFINES DT-DEPART-ADJM PIC 9(08).
              05 NU-DATE-ADD-ADJM.
                 07 NU-ADD-JJ-ADJM      PIC S9(05) VALUE ZERO.
      *          07 NU-ADD-MM-ADJM      PIC S9(05) VALUE ZERO.
      *          07 NU-ADD-AA-ADJM      PIC S9(03) VALUE ZERO.
           03 ZONE-DIRECTIVES-ADJM.
              05 C-FONCTION-ADJM        PIC X(04)  VALUE 'ADJM'.
              05 C-CALENTRIER-ADJM      PIC X(02)  VALUE 'CV'.
              05 C-VALIDITE-ADJM        PIC X(01)  VALUE '1'.
              05 C-FORMAT-DT-DEP-ADJM   PIC X(03)  VALUE 'K4E'.
              05 C-FORMAT-DT-RES-ADJM   PIC X(03)  VALUE 'K4E'.
              05 C-CAL-RES-ADJM         PIC X(02)  VALUE SPACES.
              05 BORNES-ADJM.
                 07 C-BORNE-DEP-ADJM    PIC X(1)   VALUE 'E'.
                 07 C-BORNE-RES-ADJM    PIC X(1)   VALUE 'I'.
              05 CALCULS-DATE-ADJM.
                 07 CALC-JJ-ADJM        PIC X(1)   VALUE 'E'.
                 07 CALC-MM-ADJM        PIC X(1)   VALUE SPACE.
                 07 CALC-AA-ADJM        PIC X(1)   VALUE SPACE.
              05 C-JOURS-OUV-ADJM       PIC X(1)   VALUE SPACE.
           03 ZONE-RESULTATS-ADJM.
              05 C-RET-ADJM             PIC X(03)  VALUE SPACE.
              05 DATH-RES-DT-ADJM.
                 07 DATH-RES-SS-ADJM    PIC 9(02)  VALUE ZERO.
                 07 DATH-RES-AA-ADJM    PIC 9(02)  VALUE ZERO.
                 07 DATH-RES-MM-ADJM    PIC 9(02)  VALUE ZERO.
                 07 DATH-RES-JJ-ADJM    PIC 9(02)  VALUE ZERO.

      ** GESTION RUPTURE
       COPY GDSN005A REPLACING ==(PREF-)== BY ==NEW-==.

       COPY GDSN005A REPLACING ==(PREF-)== BY ==OLD-==.
       COPY GDSN001A REPLACING ==(PREF-)== BY ==LS-==.

       COPY GDDF970A.

      ** IDMS
       COPY IDMS MMA-EXTENDED-STATISTICS.
       COPY IDMS SUBSCHEMA-NAMES.
       01  L-IDMS.
           03 COPY IDMS SUBSCHEMA-CTRL.
           03 COPY IDMS RECORD G002RB-ECON.
           03 COPY IDMS RECORD G002RB-ESC1.


       PROCEDURE DIVISION.
      ********************
           DISPLAY '*** DEBUT DU PROGRAMME GDSN015A ***'


           PERFORM INITIALISATION

      *    PREMIERE LECTURE
           PERFORM LECTURE-DDE1S

      *    LECTURE FICHIER ET TRAITEMENT
           PERFORM UNTIL WS-EOF
      *        RECUP' DATE FIN DERNIERE PERIODE DU CONTRAT EN COURS
               PERFORM GET-ESC1
               IF REC-ESC1-OK
AM1512*           IF DT-DERN-ENV-ESC1 NOT = ZEROES
AM1512            IF DT-DEB-PERIO-ESC1 NOT = ZEROES
AM1512*              MOVE DT-DERN-ENV-ESC1 TO DT-DEPART-ADJM-9
AM1512*              MOVE 1           TO NU-ADD-JJ-ADJM
AM1512*              PERFORM CALC-DT-DEB-OK
AM1512*              IF NEW-ECP-DATE-DEB-PER > WS-DT-DEB-OK
AM1512*              IF NEW-ECP-DATE-DEB-PER-NU > DT-DEB-PERIO-ESC1
                   IF   NEW-ECP-DATE-FIN-PER-NU >= DT-DEB-PERIO-ESC1
                   AND (NEW-ECP-DATE-DEB-PER-NU NOT = DT-DEB-PERIO-ESC1
                    OR  NEW-ECP-DATE-FIN-PER-NU NOT = DT-FIN-PERIO-ESC1)
                       SET WS-G97-EXIST   TO TRUE
                       INITIALIZE LS-GDSN001A
                       MOVE 'G97'         TO LS-GR-MSG-CODE-MSG
                       MOVE LIB-G97       TO LS-GR-MSG-LIB-MSG
                       PERFORM TRAITEMENT-PERIODES-NEW
                       PERFORM TRAITEMENT-NEW-KEY
                     END-IF
                  END-IF
               END-IF

               IF NEW-ECP-NU-CONT = OLD-ECP-NU-CONT
                   IF OLD-ECP-DATE-DEB-PER = NEW-ECP-DATE-DEB-PER
                    AND OLD-ECP-TYPE-FLUX NOT = NEW-ECP-TYPE-FLUX
                    AND WS-G97-NOT-EXIST
                       SET WS-G98-EXIST   TO TRUE
                       INITIALIZE LS-GDSN001A
                       MOVE 'G98'         TO LS-GR-MSG-CODE-MSG
                       MOVE LIB-G98       TO LS-GR-MSG-LIB-MSG
                       PERFORM TRAITEMENT-PERIODES-OLD
                       PERFORM TRAITEMENT-PERIODES-NEW
                       PERFORM TRAITEMENT-OLD-KEY
                       PERFORM TRAITEMENT-NEW-KEY
                   END-IF

                   IF OLD-ECP-DATE-FIN-PER >= NEW-ECP-DATE-DEB-PER
                    AND WS-G97-NOT-EXIST AND WS-G98-NOT-EXIST
                       INITIALIZE LS-GDSN001A
                       MOVE 'G99'         TO LS-GR-MSG-CODE-MSG
                       MOVE LIB-G99       TO LS-GR-MSG-LIB-MSG
                       PERFORM TRAITEMENT-PERIODES-OLD
                       PERFORM TRAITEMENT-PERIODES-NEW
                       PERFORM TRAITEMENT-OLD-KEY
                       PERFORM TRAITEMENT-NEW-KEY
                   END-IF
               END-IF
               SET WS-G97-NOT-EXIST       TO TRUE
               SET WS-G98-NOT-EXIST       TO TRUE

               PERFORM LECTURE-DDE1S
           END-PERFORM

           INITIALIZE LS-GDSN001A
           MOVE 03                        TO LS-CODE-APPEL
           MOVE 99                        TO LS-TYPE-ECR
           PERFORM APPEL-GDSNSUIV

           CLOSE FIC-ENT
           PERFORM DISPLAY-STATISTICS
           FINISH
           DISPLAY '*** FIN DU PROGRAMME GDSN015A ***'
           GOBACK
           .

       INITIALISATION.
      D    DISPLAY '015A- ** INITIALISATION **'

           OPEN INPUT   FIC-ENT

           COPY IDMS SUBSCHEMA-BINDS.
           READY G002Z001 USAGE-MODE IS RETRIEVAL.
           PERFORM DISPLAY-PROGRAMME THRU FIN-DISPLAY-PROGRAMME

           INITIALIZE WS-CPT
                      NEW-GDSN005A  OLD-GDSN005A

           SET WS-OK                      TO TRUE
           SET WS-G97-NOT-EXIST           TO TRUE
           SET WS-G98-NOT-EXIST           TO TRUE
           ACCEPT WS-DT-TRT-9             FROM SYSIN
           DISPLAY 'DATE DU TRAITEMENT : ' WS-DT-JJ '/'
                                           WS-DT-MM '/' WS-DT-SA

           INITIALIZE LS-GDSN001A
           MOVE 01                        TO LS-CODE-APPEL
           MOVE 99                        TO LS-TYPE-ECR
           PERFORM APPEL-GDSNSUIV
           .

       TRAITEMENT-NEW-KEY.
      ** POUR LIGNE EN COURS
      *    CODE APPEL 04 = RECUP INFOS CONTRAT WGCO
      *    GDAF970                        TO LS-GR-CP-GRP-GEST-COT
           INITIALIZE LS-GDSN001A
           MOVE 04                        TO LS-CODE-APPEL
           MOVE NEW-ECP-NU-CONT           TO LS-PARAM-CONT
           MOVE NEW-ECP-DATE-DEB-PER      TO LS-PARAM-DT-EFFET
           PERFORM APPEL-GDSNSUIV

      *    GDSNSUIV VIA CODE APPEL 02
           INITIALIZE LS-GDSN001A
           MOVE 02                        TO LS-CODE-APPEL
           MOVE 04                        TO LS-TYPE-ECR

           MOVE 'GPE'                     TO LS-GR-CP-CODE-REJ
           MOVE LIB-GPE                   TO LS-GR-CP-LIB-REJ
           MOVE NEW-ECP-NU-CONT           TO LS-GR-CP-CONTRAT
           MOVE NEW-ECP-DATE-DEB-PER      TO LS-GR-CP-DATE-DEB-PER
           MOVE NEW-ECP-DATE-FIN-PER      TO LS-GR-CP-DATE-FIN-PER
           MOVE NEW-PMT-REF-PMT           TO LS-GR-CP-REF-PAIEMENT
           PERFORM APPEL-GDSNSUIV
           .

       TRAITEMENT-OLD-KEY.
      ** POUR LIGNE PRECEDENTE
      *    CODE APPEL 04 = RECUP INFOS CONTRAT WGCO
      *    GDAF970                        TO LS-GR-CP-GRP-GEST-COT
           INITIALIZE LS-GDSN001A
           MOVE 04                        TO LS-CODE-APPEL
           MOVE OLD-ECP-NU-CONT           TO LS-PARAM-CONT
           MOVE OLD-ECP-DATE-DEB-PER      TO LS-PARAM-DT-EFFET
           PERFORM APPEL-GDSNSUIV

      *    GDSNSUIV VIA CODE APPEL 02
           INITIALIZE LS-GDSN001A
           MOVE 02                        TO LS-CODE-APPEL
      **  LIGNE CONTRAT/PERIODE KO
           MOVE 04                        TO LS-TYPE-ECR

           MOVE 'GPE'                     TO LS-GR-CP-CODE-REJ
           MOVE LIB-GPE                   TO LS-GR-CP-LIB-REJ

           MOVE OLD-ECP-NU-CONT           TO LS-GR-CP-CONTRAT
           MOVE OLD-ECP-DATE-DEB-PER      TO LS-GR-CP-DATE-DEB-PER
           MOVE OLD-ECP-DATE-FIN-PER      TO LS-GR-CP-DATE-FIN-PER
           MOVE OLD-PMT-REF-PMT           TO LS-GR-CP-REF-PAIEMENT
           PERFORM APPEL-GDSNSUIV
           .

       TRAITEMENT-PERIODES-NEW.
      ** POUR PERIODE EN COURS
           MOVE 02                        TO LS-CODE-APPEL
      **  LIGNE MESSAGE KO
           MOVE 06                        TO LS-TYPE-ECR
           MOVE NEW-ECP-NU-CONT           TO LS-GR-MSG-CONTRAT
           MOVE NEW-ECP-DATE-DEB-PER      TO LS-GR-MSG-DATE-DEB-PER
           MOVE NEW-ECP-DATE-FIN-PER      TO LS-GR-MSG-DATE-FIN-PER
D00670*    MOVE NEW-ADH-NIR               TO LS-GR-MSG-NIR
           PERFORM APPEL-GDSNSUIV
           .

       TRAITEMENT-PERIODES-OLD.
      ** POUR PERIODE PRECEDENTE
           MOVE 02                        TO LS-CODE-APPEL
      **  LIGNE MESSAGE KO
           MOVE 06                        TO LS-TYPE-ECR
           MOVE OLD-ECP-NU-CONT           TO LS-GR-MSG-CONTRAT
           MOVE OLD-ECP-DATE-DEB-PER      TO LS-GR-MSG-DATE-DEB-PER
           MOVE OLD-ECP-DATE-FIN-PER      TO LS-GR-MSG-DATE-FIN-PER
D00670*    MOVE OLD-ADH-NIR               TO LS-GR-MSG-NIR
           PERFORM APPEL-GDSNSUIV
           .

       GET-ESC1.
           MOVE NEW-ECP-NU-CONT           TO GR-CON-DAC-ECON

           SET REC-ECON-OK                TO TRUE
           OBTAIN CALC G002RB-ECON
           ON DB-REC-NOT-FOUND
               SET REC-ECON-KO            TO TRUE
           END-IF

           IF REC-ECON-OK
               SET REC-ESC1-OK            TO TRUE
               OBTAIN LAST G002RB-ESC1 WITHIN G002J-ECON-ESPC
               ON DB-END-OF-SET
                   SET REC-ESC1-KO        TO TRUE
               END-IF
           END-IF
           .

      *-------------------------------------------
      *  AJOUT D'UN NOMBRE DE JOURS
      *-------------------------------------------
       CALC-DT-DEB-OK.
      *-------------------------------------------
      D    DISPLAY '015A- * CALC-DT-DEB-OK *'

           CALL  'DATHEUR'   USING   ZONE-DIRECTIVES-ADJM
                                     ZONE-DONNEE-ADJM
                                     ZONE-RESULTATS-ADJM

           IF C-RET-ADJM NOT = ZERO
              DISPLAY '015A- **************************************'
              DISPLAY '  GDSN015A - PARAGRAPHE : CALC-DT-DEB-OK'
              DISPLAY '- PB DAT�ADJM - CODE RETOUR  : ' C-RET-ADJM
              DISPLAY '- DATE DEPART  : ' DT-DEPART-ADJM
              DISPLAY '- NB DE JOURS  : ' NU-ADD-JJ-ADJM
              DISPLAY '015A- **************************************'
           END-IF

           MOVE DATH-RES-JJ-ADJM          TO WS-DEB-OK-JJ
           MOVE DATH-RES-MM-ADJM          TO WS-DEB-OK-MM
           MOVE DATH-RES-SS-ADJM          TO WS-DEB-OK-SS
           MOVE DATH-RES-AA-ADJM          TO WS-DEB-OK-AA
           .

       APPEL-GDSNSUIV.
      D    DISPLAY '015A- ** APPEL-GDSNSUIV **'

           CALL GDSNSUIV USING LS-GDSN001A GDDF970A
           .

       LECTURE-DDE1S.
      D    DISPLAY '015A- ** LECTURE-DDE1S **'

      *    STOCKAGE DE L'ANCIENNE CLE
           MOVE NEW-GDSN005A              TO OLD-GDSN005A

      *    LECTURE DE LA NOUVELLE CLE
           READ FIC-ENT                   INTO NEW-GDSN005A
               AT END
                   SET WS-EOF             TO TRUE
               NOT AT END
                   ADD 1                  TO WS-CPT-LIGNE
           END-READ
           .

       ABANDON SECTION.

           DISPLAY ' '
           DISPLAY ' A B A N D O N   DE  GDSN015A'
           DISPLAY ' '
           DISPLAY ' (LIGNE ' WS-CPT-LIGNE ')'
           DISPLAY ' '
           CALL 'ABANDUMP'
           .

       IDMS-ABORT.
           PERFORM DISPLAY-STATISTICS
           EXIT
           .
           COPY IDMS MMA-DISPLAY-STATISTICS.
           COPY IDMS IDMS-STATUS.

