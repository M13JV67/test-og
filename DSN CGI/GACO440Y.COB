      *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    GACO440Y.
       AUTHOR.        THIBAUD GAHAGNON.
       DATE-WRITTEN.  JANVIER 2016.
       DATE-COMPILED.
      *
      *****************************************************************
      *                                                               *
      *                      G A C O 4 4 0 Y                          *
      *                                                               *
      *****************************************************************
      *   SOUS PROGRAMME PERMETTANT L'ECRITURE DU FICHIER CSV         *
      *                                                               *
      *****************************************************************
      *          *   - GAOC440Y : COPIE DE PILOTAGE DU MODULE         *
      *  COPIES  *   - GAOC440K : BLOC DONEES                         *
      *          *                                                    *
      *****************************************************************
      * FICHIERS *   -    DDS8S : FICHIER CSV                         *
      *          *                                                    *
      *****************************************************************
      *          *                                                    *
      *   CODE   *           01 : OUVERTURE DU FICHIER                *
      *          *           05 : ECRITURE LIGNE                      *
      *   APPEL  *           99 : FERMETURE DU FICHIER                *
      *          *                                                    *
      *****************************************************************
      *                       MODIFICATIONS                           *
      *---------------------------------------------------------------*
      *    DATE    |  NOM  |               DESCRIPTION                *
      *---------------------------------------------------------------*
      *  28/01/16  |  TGA  |   CREATION DU PROGRAMME                  *
      *---------------------------------------------------------------*
TG1602*  16/02/16  |  TGA  |   MODIFICATION CALCUL LONGUEUR ELEMENT   *
TG1602*            |       |   CSV ( LONGUEUR ERRONEE SI DEUX ESPACES *
TG1602*            |       |   CONSECUTIFS EN MILIEU DE CHAINE        *
TG1602*---------------------------------------------------------------*

       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
      *---------------------*
       OBJECT-COMPUTER. IBM-3090.
      *SOURCE-COMPUTER. IBM-3090.
       SOURCE-COMPUTER. IBM-3090 WITH DEBUGGING MODE.

       INPUT-OUTPUT SECTION.
      *--------------------*
       FILE-CONTROL.

      *    E1 : FLUX CSV
           SELECT FIC-CSV ASSIGN TO DDS8S.

       DATA DIVISION.
      *-------------*
       FILE SECTION.

      * S1 : FICHIER CSV
       FD FIC-CSV
            BLOCK CONTAINS 0 RECORDS
            RECORDING MODE V.

       01 ENR-CSV.
          05 ENR-CSVT   PIC X(1) OCCURS 2000 DEPENDING ON W-LG-CSV.


       WORKING-STORAGE SECTION.
      *-----------------------*

      *
      * ZONES COMPTEURS / INDEX
      *
       01 WS-CPT.
           03 WI-A                       PIC 9(08) COMP.
           03 WS-I                       PIC 9(08) COMP.
      *
      * ZONE LONGUEUR FICHIERS
      *
       01 WS-LG-FIC.
           05 W-LG-CSV                   PIC 9(08) COMP VALUE 2000.

      *
      *  GESTION STATUS FICHIER
      *
      *01 WS-FLAG.

      *
      * ZONES DE TRAVAIL
      *
       01 WS-DATA.
           05 WS-LIG-CSV                 PIC X(2000).

      *
      * ZONES DE TRAVAIL CSV
      *
       01 W-VAR-AJOUT-ED.
          05 W-VAR-AJOUT                 PIC X(1)
                             OCCURS 1 TO 2000 DEPENDING ON W-VAR-INDX.
       01 W-VAR-GR-TT.
          05 W-VAR-TEMP                  PIC X(2000).
          05 W-VAR-NUM                   PIC +(8)9,9(2).
          05 W-VAR-EDIT                  PIC X(1) VALUE ';'.
          05 W-VAR-PT                    PIC 9(6) COMP.
          05 W-VAR-INDX                  PIC 9(6) COMP.
       01 W-DONNEE                       PIC X(2000).
      * LONGUEUR MAXIMALE D'UN ELEMENT CSV
TG1602 01 W-LONG-MAX                     PIC 9(4) VALUE 50.


      *
      * ZONE COPIES
      *
       COPY GAOC440K REPLACING ==(PREF-)== BY ==LS-==.


       LINKAGE SECTION.
      *---------------*

      *
      * ZONE COPIES
      *
       COPY GAOC440Y REPLACING ==(PREF-)== BY ==LS-==.


       PROCEDURE DIVISION USING LS-GAOC440Y.
      *************************************

      D    DISPLAY '440Y- LS-C-APP-CSV : ' LS-C-APP-CSV

TG1602     MOVE 50                       TO W-LONG-MAX

           EVALUATE LS-C-APP-CSV
      *
      *      CODE APPEL 01 : PROCEDURE D'INITIALISATION
      *
             WHEN '01'
               PERFORM APP-INI
      *
      *      CODE APPEL 05 : PROCEDURE D'ECRITURE DE LIGNE
      *
             WHEN '05'
               PERFORM APP-LIG
      *
      *      CODE APPEL 99 : PROCEDURE DE FINALISATION
      *
             WHEN '99'
               PERFORM APP-FIN

             WHEN OTHER
      D        DISPLAY '440Y- || --> CODE APPEL INCORRECT'
               CONTINUE
           END-EVALUATE

      D    DISPLAY '440Y- FIN APPEL-GACO440Y'
           GOBACK
           .

      *----------------------------------------------------------------*

      *
      *   CODE 01 : PROCEDURE D'INITIALISATION
      *
       APP-INI.
      D    DISPLAY '440Y- INITIALISATION (CODE APPEL: 01)'
      *    MOVE 'GACO440Y'                    TO PROGRAM-NAME
           INITIALIZE WS-CPT

      *    OUVERTURE DU FICHIER CSV
           OPEN OUTPUT FIC-CSV

           .

      *
      *   CODE 99 : PROCEDURE DE FINALISATION
      *
       APP-FIN.
      D    DISPLAY '440Y- FINALISATION (CODE APPEL: 99)'

      *    FERMETURE DU FICHIER CSV
           CLOSE FIC-CSV

           .

      *
      *  CODE 05 : ECRITURE D'UNE LIGNE DANS LE FICHIER CSV
      *
       APP-LIG.
      D    DISPLAY '440Y- ECRITURE LIGNE CSV (CODE APPEL: 02)'

      *    RECUPREATION DES DONNEES A ECRIRE
           MOVE LS-DATA-CSV                   TO GAOC440K

      *    MISE EN FORME DE LA LIGNE EN CSV
           PERFORM MEF-CSV

      *    CALCUL LONGUEUR LIGNE
           MOVE W-VAR-PT                      TO W-LG-CSV

      *    ECRITURE LIGNE
           WRITE ENR-CSV FROM WS-LIG-CSV
           .

      *-----------------------------------------------------------------
      *                    CREATION LIGNE CSV
      *-----------------------------------------------------------------

      *
      *  CREATION DE LA ZONE CSV
      *
       MEF-CSV.
      D    DISPLAY '440Y- CREATION CSV'
      *    INITIALISATION
           MOVE SPACES                        TO W-VAR-TEMP
           MOVE 1                             TO W-VAR-PT

      *    ALIMENTATION LIGNE CSV
           MOVE LS-ORIG                       TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-CODE-DSN                   TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-NOM-ENTR                   TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR1                       TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR2                       TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR3                       TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-CP                         TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-VILLE                      TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-CONT                   TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-PROD                   TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-ORIAS                  TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-ENCAIS                 TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-SIRET                  TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-DATE-JOUR                  TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-DAT-DEB                TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-DAT-FIN                TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-DER-MOIS               TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-CHQ                    TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-NUMERO                     TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-NOM-APPORT                 TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR1-APPORT                TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR2-APPORT                TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-ADR3-APPORT                TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-CP-APPORT                  TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-VILLE-APPORT               TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-PAYS                       TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-SOUSC                  TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-REF-APPORT                 TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-EXT                        TO W-DONNEE
           PERFORM MEF-CSV-ELEM
           MOVE LS-C-DON28                    TO W-DONNEE
           PERFORM MEF-CSV-ELEM

      *    ALIMENTATION DE LA DONNEE DU FICHIER DE SORTIE
           MOVE W-VAR-TEMP                    TO WS-LIG-CSV
           .

      *
      *    CREATION D'UNE LIGNE CSV
      *
TG1602 MEF-CSV-ELEM.
TG1602     MOVE W-LONG-MAX                    TO WS-I
TG1602     MOVE W-LONG-MAX                    TO W-VAR-INDX
TG1602     MOVE W-DONNEE                      TO W-VAR-AJOUT-ED
TG1602
TG1602     PERFORM UNTIL WS-I = ZERO
TG1602       IF W-VAR-AJOUT(WS-I) = SPACES
TG1602         SUBTRACT 1                   FROM WS-I
TG1602         SUBTRACT 1                   FROM W-VAR-INDX
TG1602       ELSE
TG1602         MOVE WS-I                      TO W-VAR-INDX
TG1602         MOVE ZERO                      TO WS-I
TG1602       END-IF
TG1602     END-PERFORM
TG1602     PERFORM D-DELIMITED
TG1602     .
TG1602
TG1602*MEF-CSV-ELEM.
TG1602*    MOVE ZEROES                        TO W-VAR-INDX
TG1602*
TG1602*    INSPECT W-DONNEE                   TALLYING W-VAR-INDX
TG1602*                                       FOR CHARACTERS BEFORE '  '
TG1602*    MOVE W-DONNEE                      TO W-VAR-AJOUT-ED
TG1602*    PERFORM D-DELIMITED
TG1602*    .

       D-DELIMITED.
           STRING W-VAR-AJOUT-ED
                  W-VAR-EDIT                  DELIMITED BY SIZE
             INTO W-VAR-TEMP                  WITH POINTER W-VAR-PT
           END-STRING
           .

      *----------------------------------------------------------------*

       ABANDON.

           DISPLAY ' '
           DISPLAY ' A B A N D O N   DE  GACO440Y'
           DISPLAY ' '
           CALL 'ABANDUMP'
           .

