       IDENTIFICATION DIVISION.
       PROGRAM-ID.    GDSNSUIV.
       AUTHOR.        AMA.
       DATE-WRITTEN.  JUILLET 2015.
       DATE-COMPILED.

      *************************************************************
      *                                                           *
      *    CREATION FICHIER COMPTE RENDU / SUIVI                  *
      *                                                           *
      *    + FICHIER EN ENTREE :                                  *
      *    DDE99S : FICHIER SUIVI ENTREE                          *
      *                                                           *
      *    + FICHIER EN SORTIE :                                  *
      *    DDS99S : FICHIER SUIVI SORTIE                          *
      *                                                           *
      *************************************************************
      * 15/07/15 *      CREATION DU PROGRAMME   MANTIS 1854       *
      *   AMA    *                                                *
      *************************************************************
      *   HISTORIQUE DES MODIFICATIONS                            *
      *************************************************************
      * 23/09/15 *   RETOUR CERTIFICATION                         *
      *   YMG    *   YG1509                                       *
      *   QUAND ON TRAITE UNE ANOMALIE DU FLUX DE GESTION         *
      *   ON CREE LE CODE APPEL 7                                 *
      *************************************************************
      * 14/10/15 *   RETOUR CERTIFICATION LOT 3                   *
      *   RLA    *   RL1510                                       *
      *   MISE A JOUR EFFECTIVE DE LA LIGNE DE SYNTHESE           *
      * + GESTION DE SON ABSENCE                                  *
      * + SIMPLIFICATION DES FUTURES MAINTENANCES SI CHANGEMENT   *
      *   DE TAILLE DE LA COPY GDSNSUIV                           *
      *************************************************************
      * 30/12/15 *   RECETTE DSN        AM1512                    *
      *   AMA    *   GESTION DU CODE NATURE MESSAGE               *
      *************************************************************

       ENVIRONMENT DIVISION.
      **********************
       CONFIGURATION SECTION.
      *---------------------*
       OBJECT-COMPUTER. IBM-3090.
       SOURCE-COMPUTER. IBM-3090.
      *SOURCE-COMPUTER. IBM-3090 WITH DEBUGGING MODE.

       INPUT-OUTPUT SECTION.
      *--------------------*
       FILE-CONTROL.
           SELECT  FIC-ENT ASSIGN TO  DDE99S.
           SELECT  FIC-SOR ASSIGN TO  DDS99S.

       DATA DIVISION.
      *-------------*
       FILE SECTION.

      * FICHIER SUIVI
       FD FIC-ENT
            RECORDING MODE IS F
            LABEL RECORDS STANDARD
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-ENT                     PIC X(400).

      * FICHIER SUIVI
       FD FIC-SOR
            RECORDING MODE IS F
            LABEL RECORDS STANDARD
            BLOCK CONTAINS 0 RECORDS.
       01 F-ENR-SOR                     PIC X(400).


       WORKING-STORAGE SECTION.
      *-----------------------*
      * ZONES COMPTEURS
       01 WS-CPT.
           03 WS-CPT-LIGNE-E99          PIC 9(08).
           03 WS-CPT-ENR-TYP2           PIC 9(08).
           03 WS-CPT-ENR-TYP3           PIC 9(08).

RL1510* ECRITURE SORTIE
RL1510*01 WS-ENR-SYN                    PIC X(400).

      * GESTION ETAT FICHIER
       01 WS-ETAT-FICHIER               PIC X(01).
           88 WS-EOF                               VALUE 'O'.
           88 WS-OK                                VALUE 'N'.

      * ZONES VALEURS FIXES
       01 WS-FIX-VALUE.
      *    LIBELLES ANOMALIES
           03 SY-LIB1.
               05 FILLER                PIC X(11)  VALUE 'Le message '.
               05 LIB1-REF-MSG          PIC X(35).
               05 FILLER                PIC X(37)  VALUE
                               ' a {t{ rejet{ pour le motif suivant "'.
               05 LIB1-REJ1             PIC X(05).
               05 FILLER                PIC X(13)  VALUE
                                                       '" @ la ligne '.
               05 LIB1-LIG1             PIC 9(08).
               05 WS-QTE-REJ1           PIC X(20)  VALUE SPACES.
           03 SY-LIB2.
               05 FILLER                PIC X(47)  VALUE
                     'Le message a {t{ rejet{ pour le motif suivant "'.
               05 LIB2-REJ1             PIC X(05).
               05 FILLER                PIC X(13)  VALUE
                                                       '" @ la ligne '.
               05 LIB2-LIG              PIC 9(08).
               05 WS-QTE-REJ2           PIC X(20)  VALUE SPACES.

      *--> ZONE DE LIAISON AVEC GDAF970A
       01  GDAF970                      PIC  X(08) VALUE 'GDAF970 '.

       COPY GDSNSUIV REPLACING ==(PREF-)== BY ==E99-==.
       COPY GDSNSUIV REPLACING ==(PREF-)== BY ==S99-==.
RL1510 COPY GDSNSUIV REPLACING ==(PREF-)== BY ==SYN-==.


       LINKAGE SECTION.
      *---------------*
       COPY GDSN001A REPLACING ==(PREF-)== BY ==LS-==.
       COPY GDDF970A.


       PROCEDURE DIVISION USING LS-GDSN001A GDDF970A.
      ********************


           EVALUATE LS-CODE-APPEL
      *     CODE APPEL 01 = RECOPIER ET OUVRIR FICHIERS
            WHEN 01
                PERFORM INITIALISATION

                PERFORM LECTURE-DDE99S

      *         BOUCLE SUR LE FICHIER D'ENTREE
                PERFORM UNTIL WS-EOF
      *             SAUVEGARDE DE LA LIGNE EXISTANTE
                    MOVE E99-GDSNSUIV         TO S99-GDSNSUIV
RL1510*             PERFORM SAUV-SYNTHESE
RL1510              IF E99-TYPE-ENREG = 1
RL1510D                DISPLAY 'SUIV- ** SAUV-SYNTHESE **'
RL1510                 MOVE E99-GDSNSUIV      TO SYN-GDSNSUIV
RL1510              ELSE
                       PERFORM ECRITURE-DDS99S
RL1510              END-IF

                    PERFORM LECTURE-DDE99S
                END-PERFORM

      *     CODE APPEL 02 = TRAITER DONNEES
            WHEN 02
                EVALUATE LS-TYPE-ECR
      *          TYPE ECRITURE 01 = LIGNE DE SYNTHESE OK
      *          TYPE ECRITURE 02 = LIGNE DE SYNTHESE KO
                 WHEN 01
                 WHEN 02
                     PERFORM GESTION-SYNTHESE
RL1511*              PERFORM ECRITURE-DDS99S
      *          TYPE ECRITURE 03 = LIGNE DE CONTRAT/PERIODE OK
      *          TYPE ECRITURE 04 = LIGNE DE CONTRAT/PERIODE KO
                 WHEN 03
                 WHEN 04
                     PERFORM GESTION-CONTRAT-PERIODE
                     PERFORM ECRITURE-DDS99S
      *          TYPE ECRITURE 05 = LIGNE DE MESSAGE OK
      *          TYPE ECRITURE 06 = LIGNE DE MESSAGE KO
                 WHEN 05
                 WHEN 06
                     PERFORM GESTION-MESSAGE
                     PERFORM ECRITURE-DDS99S
AM1511* CE TYPE-ECR N'EST PAS NECESSAIRE
AM1511* CAR ON A LA DONNEE LS-GR-SYN-TYPE-FLUX
YG1509**         SPECIFIQUE CONTROLE FLUX DE GESTION
YG1509**         WHEN 07
YG1509**             PERFORM GESTION-SYNTHESE
RL1511*              PERFORM ECRITURE-DDS99S
                 WHEN OTHER
                     CONTINUE
                END-EVALUATE

      *     CODE APPEL 03 = FERMER FICHIERS
            WHEN 03
      *         FERMETURE GDAF970
                IF LS-TYPE-ECR = 99
                    INITIALIZE GDDF970A
                    MOVE '23'                 TO CODE-APP-TRT
                    PERFORM APPEL-GDAF970A
                END-IF

RL1510          IF SYN-TYPE-ENREG = 1
                   PERFORM GESTION-COMPTEUR
RL1510             PERFORM ECRITURE-DDS99S
RL1510          END-IF
                CLOSE FIC-ENT
                CLOSE FIC-SOR

      *     CODE APPEL 04 = RECUP INFOS CONTRAT WGCO VIA GDAF970
            WHEN 04
                INITIALIZE GDDF970A
                MOVE LS-PARAM-CONT            TO GR-CON-DAC-TRT
                MOVE '22'                     TO CODE-APP-TRT
                MOVE 'D'                      TO MODE-FONC-TRT
                MOVE LS-PARAM-DT-EFFET-9      TO DT-EFF-DDE-TRT
                PERFORM APPEL-GDAF970A

                IF RETOUR-GEN-TRT = '0'
                   MOVE NU-GRO-GES-WCON (1)  TO LS-GR-CP-GRP-GEST-COT
                END-IF

            WHEN OTHER
                CONTINUE
           END-EVALUATE

      *    DISPLAY 'SUIV- *** FIN APPEL-GDSNSUIV ***'
           GOBACK
           .

       INITIALISATION.
      D    DISPLAY 'SUIV- ** INITIALISATION **'

           OPEN INPUT   FIC-ENT
           OPEN OUTPUT  FIC-SOR

           SET WS-OK                          TO TRUE

RL1510     INITIALIZE WS-CPT  S99-GDSNSUIV  SYN-SY
RL1510     MOVE 0                             TO SYN-TYPE-ENREG
RL1510*    MOVE SPACE                         TO WS-ENR-SYN
RL1510*    INITIALIZE WS-CPT  S99-GDSNSUIV

      *    OUVERTURE GDAF970A
           IF LS-TYPE-ECR = 99
               INITIALIZE GDDF970A
               MOVE '21'                      TO CODE-APP-TRT
               PERFORM APPEL-GDAF970A
           END-IF
           .

RL1510*SAUV-SYNTHESE.
RL1510*    DISPLAY 'SUIV- ** SAUV-SYNTHESE **'
RL1510*
RL1510*    IF E99-TYPE-ENREG = 01
RL1510*        MOVE E99-GDSNSUIV              TO WS-ENR-SYN
RL1510*    END-IF
RL1510*    .

       GESTION-COMPTEUR.
      D    DISPLAY 'SUIV- ** GESTION-COMPTEUR **'
RL1510*    MOVE WS-ENR-SYN                    TO S99-GDSNSUIV
RL1510     MOVE SYN-GDSNSUIV                  TO S99-GDSNSUIV
           ADD  WS-CPT-ENR-TYP2               TO S99-SY-NB-ENR-TYP2
           ADD  WS-CPT-ENR-TYP3               TO S99-SY-NB-ENR-TYP3
           .

       GESTION-SYNTHESE.
      D    DISPLAY 'SUIV- ** GESTION-SYNTHESE **'

RL1510     INITIALIZE S99-SY
RL1510*    INITIALIZE S99-GDSNSUIV         S99-SY
RL1510*               S99-SY-MESSAGE       WS-ENR-SYN

      *    CONSTRUCTION LIGNE SYNTHESE
           MOVE 1                             TO S99-TYPE-ENREG
           MOVE 1                             TO S99-SY-EMET-CR

AM1511* MISE EN COMMENTAIRE CAR ON ENLEVE LE TYPE-ECR = 7
YG1509*    IF LS-TYPE-ECR = 2
YG1509*       MOVE 1                          TO S99-SY-TYPE-FLUX
YG1509*    ELSE
YG1509*       MOVE ZERO                       TO S99-SY-TYPE-FLUX
YG1509*    END-IF
AM1511     MOVE LS-GR-SYN-TYPE-FLUX           TO S99-SY-TYPE-FLUX

           MOVE LS-GR-SYN-DATE-TRAIT          TO S99-SY-DATE-TRAIT
      *    LIGNE SYNTHESE OK
           IF LS-TYPE-ECR = 01
               MOVE 0                         TO S99-SY-CR-GLOBAL
           END-IF
      *    LIGNE SYNTHESE KO
      *      TYPE = 2
           IF LS-TYPE-ECR = 02
               MOVE 1                         TO S99-SY-CR-GLOBAL

               EVALUATE TRUE
      *         COMPTE RENDU SUR FLUX MODE DE GESTION
                WHEN LS-GR-SYN-TYPE-FLUX = 0
                   MOVE LS-GR-SYN-REJ1        TO LIB2-REJ1
                   MOVE LS-GR-SYN-LIG         TO LIB2-LIG
                   MOVE SPACES                TO WS-QTE-REJ2
                   IF LS-GR-SYN-NB-REJ = 1
                      MOVE '.'                TO WS-QTE-REJ2
                      MOVE SY-LIB2            TO S99-SY-MESSAGE
                   END-IF
                   IF LS-GR-SYN-NB-REJ > 1
                      MOVE ' - plusieurs rejets.' TO WS-QTE-REJ2
                      MOVE SY-LIB2            TO S99-SY-MESSAGE
                   END-IF
      *         COMPTE RENDU SUR FLUX DSN
                WHEN LS-GR-SYN-TYPE-FLUX = 1
                   MOVE LS-GR-SYN-REF-MSG     TO LIB1-REF-MSG
                   MOVE LS-GR-SYN-REJ1        TO LIB1-REJ1
                   MOVE LS-GR-SYN-LIG         TO LIB1-LIG1
                   MOVE SPACES                TO WS-QTE-REJ1
                   IF LS-GR-SYN-NB-REJ = 1
                      MOVE '.'                TO WS-QTE-REJ1
                      MOVE SY-LIB1            TO S99-SY-MESSAGE
                   END-IF
                   IF LS-GR-SYN-NB-REJ > 1
                      MOVE ' - plusieurs rejets.' TO WS-QTE-REJ1
                      MOVE SY-LIB1            TO S99-SY-MESSAGE
                   END-IF
                WHEN OTHER
                   CONTINUE
               END-EVALUATE
           END-IF
           MOVE ZEROES                        TO S99-SY-NB-ENR-TYP2
           MOVE ZEROES                        TO S99-SY-NB-ENR-TYP3
RL1510     MOVE S99-GDSNSUIV                  TO SYN-GDSNSUIV
RL1510*    MOVE S99-GDSNSUIV                  TO WS-ENR-SYN
           .

       GESTION-CONTRAT-PERIODE.
      D    DISPLAY 'SUIV- ** GESTION-CONTRAT-PERIODE **'

           INITIALIZE S99-GDSNSUIV
                      S99-CP
           ADD  1                             TO WS-CPT-ENR-TYP2
           MOVE 2                             TO S99-TYPE-ENREG
           MOVE LS-GR-CP-CONTRAT              TO S99-CP-CONTRAT
           MOVE LS-GR-CP-DATE-DEB-PER         TO S99-CP-DATE-DEB-PER
           MOVE LS-GR-CP-DATE-FIN-PER         TO S99-CP-DATE-FIN-PER
           IF LS-TYPE-ECR = 03
               MOVE 0                         TO S99-CP-CODE-NAT
           END-IF
           IF LS-TYPE-ECR = 04
               MOVE 1                         TO S99-CP-CODE-NAT
           END-IF
           MOVE LS-GR-CP-CODE-REJ             TO S99-CP-CODE-REJ
           MOVE LS-GR-CP-LIB-REJ              TO S99-CP-LIB-REJ
           MOVE LS-GR-CP-GRP-GEST-COT         TO S99-CP-GRP-GEST-COT
           MOVE LS-GR-CP-REF-PAIEMENT         TO S99-CP-REF-PAIEMENT
           .

       GESTION-MESSAGE.
      D    DISPLAY 'SUIV- ** GESTION-MESSAGE **'

           INITIALIZE S99-GDSNSUIV
                      S99-MSG
           ADD  1                             TO WS-CPT-ENR-TYP3
           MOVE 3                             TO S99-TYPE-ENREG
           MOVE LS-GR-MSG-CONTRAT             TO S99-MSG-CONTRAT
           MOVE LS-GR-MSG-DATE-DEB-PER        TO S99-MSG-DATE-DEB-PER
           MOVE LS-GR-MSG-DATE-FIN-PER        TO S99-MSG-DATE-FIN-PER
AM1512*    MOVE LS-GR-MSG-CODE-NAT            TO S99-MSG-CODE-NAT
      *
      *    TYPE ECRITURE 05 = LIGNE DE MESSAGE OK
      *
           IF LS-TYPE-ECR = 05
AM1512         MOVE ZERO                      TO S99-MSG-CODE-NAT
YG1509         MOVE LS-GR-MSG-CODE-MSG        TO S99-MSG-CODE-MSG
YG1509         MOVE LS-GR-MSG-LIB-MSG         TO S99-MSG-LIB-MSG
YG1509*        MOVE SPACES                    TO S99-MSG-CODE-MSG
YG1509*        MOVE SPACES                    TO S99-MSG-LIB-MSG
           END-IF
      *
      *    TYPE ECRITURE 06 = LIGNE DE MESSAGE KO
      *
           IF LS-TYPE-ECR = 06
AM1512         MOVE 1                         TO S99-MSG-CODE-NAT
               MOVE LS-GR-MSG-CODE-MSG        TO S99-MSG-CODE-MSG
               MOVE LS-GR-MSG-LIB-MSG         TO S99-MSG-LIB-MSG
           END-IF
           MOVE LS-GR-MSG-NIR                 TO S99-MSG-NIR
           .

       APPEL-GDAF970A.
      D    DISPLAY 'SUIV- ** APPEL-GDAF970A **'

           CALL  GDAF970   USING GDDF970A.

           EVALUATE TRUE
            WHEN RETOUR-GEN-TRT NOT = '0' AND NOT = '2'
                DISPLAY ' '
                DISPLAY 'SUIV- *=================================*'
                DISPLAY 'SUIV- * ANOMALIE ACCES BASE CONTRAT'
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- *  CODE APPEL = ' CODE-APP-TRT
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- * PARAMETRES DE L''APPEL : '
                DISPLAY 'SUIV- *        MODE = ' MODE-FONC-TRT
                DISPLAY 'SUIV- *  DATE EFFET = ' DT-EFF-DDE-TRT
                DISPLAY 'SUIV- *   SITUATION = ' NU-SIT-DDE-TRT
                DISPLAY 'SUIV- *     CONTRAT = ' GR-CON-DAC-TRT
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- * CODE RETOUR = ' RETOUR-GEN-TRT
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- * ENTITES DEMANDEES / CODE RETOUR :'
                DISPLAY 'SUIV- *     CONTRAT = ' RENS-CON-TRT
                DISPLAY 'SUIV- * SITU. QUIT. = ' RENS-QUI-TRT ' / '
                                                 RETOUR-QUI-TRT
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- *=================================*'
                DISPLAY ' '
                DISPLAY ' '

                PERFORM ABANDON
            WHEN  RETOUR-GEN-TRT = '2'
                DISPLAY ' '
                DISPLAY 'SUIV- *=================================*'
                DISPLAY 'SUIV- * ANOMALIE SUITE ACCES BASE CONTRAT'
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- * CONTRAT ABSENT = ' GR-CON-DAC-TRT
                DISPLAY 'SUIV- *'
                DISPLAY 'SUIV- *=================================*'
                DISPLAY ' '
                DISPLAY ' '
                MOVE SPACE                    TO LS-GR-CP-GRP-GEST-COT
            WHEN OTHER
                CONTINUE
           END-EVALUATE
           .

       LECTURE-DDE99S.
      D    DISPLAY 'SUIV- ** LECTURE-DDE99S **'
           READ FIC-ENT                       INTO E99-GDSNSUIV
               AT END
                   SET WS-EOF                 TO TRUE
               NOT AT END
                   ADD 1                      TO WS-CPT-LIGNE-E99
           END-READ
           .

       ECRITURE-DDS99S.
      D    DISPLAY 'SUIV- ** ECRITURE-DDS99S ** ' S99-TYPE-ENREG
           WRITE F-ENR-SOR                    FROM S99-GDSNSUIV
           .

       ABANDON.

           DISPLAY ' '
           DISPLAY ' A B A N D O N   DE  GDSNSUIV'
           DISPLAY ' '
           DISPLAY ' PARAMETRES APPEL GDSNSUIV    ' LS-PARAMETRES
           DISPLAY ' NOMBRE DE LIGNES DDE99S LUES ' WS-CPT-LIGNE-E99
           DISPLAY ' '
           CALL 'ABANDUMP'
           .

